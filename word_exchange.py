import sys
def read_file_to_list(file_path):
    with open(file_path, 'r') as file:
        lines = file.readlines()
        words = [line.strip() for line in lines]
    return words

def filtered(word_list, word_ini, word_fin):
    return [word for word in word_list if word[0] == word_ini[0] or word[0] == word_fin[0]]

def word_path(new_list, s, final_word):
    temp_word = ""
    new_word = s
    num = 0
    previous = set()
    while new_word != final_word:
        for ind in range(len(s)):
            temp_word = new_word[:ind] + final_word[ind] + new_word[ind + 1:]
            if temp_word in previous:
                continue
            if temp_word in new_list:
                new_word = temp_word
                previous.add(new_word)
                break
        num += 1
        print(new_word, num)
def word_conversion(initial_word: str, final_wrd: str, file_path: str):
    words = read_file_to_list (file_path)
    new_list = filtered(words, initial_word, final_wrd)
    word_path(new_list, initial_word, final_wrd)
word_conversion (sys.argv[1], sys.argv[2], sys.argv[3] )

