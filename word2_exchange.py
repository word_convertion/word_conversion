from collections import deque

def load_dictionary(file_path):
    with open(file_path, 'r') as file:
        words = set(word.strip().upper() for word in file.readlines())
    return words

def is_valid_word(word, dictionary):
    return word in dictionary

def get_neighbors(word, dictionary):
    neighbors = []
    for i in range(len(word)):
        for c in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ':
            if c != word[i]:
                new_word = word[:i] + c + word[i+1:]
                if new_word in dictionary:
                    neighbors.append(new_word)
    return neighbors

def word_ladder(start_word, end_word, dictionary):
    if start_word == end_word:
        return [start_word]

    queue = deque([(start_word, [start_word])])
    visited = set([start_word])

    while queue:
        current_word, path = queue.popleft()
        for neighbor in get_neighbors(current_word, dictionary):
            if neighbor not in visited:
                if neighbor == end_word:
                    return path + [end_word]
                queue.append((neighbor, path + [neighbor]))
                visited.add(neighbor)
    
    return None


file_path = 'sowpods.txt' 
start_word = 'EYE'
end_word = 'SPY'

dictionary = load_dictionary(file_path)
result = word_ladder(start_word, end_word, dictionary)

if result:
    print(" -> ".join(result))
else:
    print("No transformation sequence found.")
